class Actions {

    deleteCookies() {
        var cookies = browser.getCookie()
        return cookies.forEach(function(cookie) {
            browser.deleteCookie(cookie.name);
        });
    };
    
    getCookies() {
        let listOfCookies = [];
        var cookies = browser.getCookie();
        cookies.forEach(function(cookie) {
            listOfCookies.push(cookie.name);
        });
        return listOfCookies;
    };
        
    getPageTitle() {
        return browser.getTitle();
    };
    
    getPageUrl() {
        return browser.getUrl();
    };
    
    getElementAttribute(element, attribute) {
        return browser.getAttribute(element.selector, attribute);
    };
};

module.exports = new Actions;