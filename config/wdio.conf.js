exports.config = {
    specs: [
        './tests/*.spec.js'
    ],
    maxInstances: 10,
    capabilities: [
        {
            browserName: 'chrome',
            'chromeOptions': {
                args: ['--start-fullscreen']
            },
        }
    ],
    sync: true,
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'error',
    coloredLogs: true,
    deprecationWarnings: true,
    bail: 0,
    screenshotPath: './errorShots/',
    baseUrl: 'https://www.solenenergy.com',
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    services: ['selenium-standalone','screenshots-cleanup'],
    cleanScreenshotsFolder: {
        folder: 'errorShots',
        pattern: '*.png'
    },
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd'
    },
    afterSession: async function(){
        // workaround to make sure the chromedriver shuts down
        // https://github.com/webdriverio/wdio-selenium-standalone-service/issues/28
        await browser.end().pause(1000);
    },
    after: async function(){
        // workaround to make sure the chromedriver shuts down
        // https://github.com/webdriverio/wdio-selenium-standalone-service/issues/28
        await browser.pause(1000);
    }
}
