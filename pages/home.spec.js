const waitUntil = require("../utils/waits");
const menu = require("./menu.spec");

class HomePage {
    
    open() {
        browser.url("/");
        return waitUntil.elementIsVisible(menu.imgLogo);
    };
};

module.exports = new HomePage();