const waitUntil = require("../utils/waits");

class ListingPage {
    get textHeader() { return $(".h1-holder"); } 
    get textNoResults() { return $(".msg-block"); }
    get textResults() { return $(".counter-inside"); }
    get textSearchTerm() { return $(".combined-description"); }
    get buttonAddToBasketTop() { return $("#btnQuickOrderTop"); }
    get buttonAddToBasketBottom() { return $("#btnQuickOrderBottom"); }
    get divProducts() { return $$(".l-products-item"); }
    
    open() {
        browser.url("/search");
        return waitUntil.elementIsVisible(this.textHeader);
    };

    numberOfProductsPerPage() { 
        return this.divProducts.length;
    };

    numberOfResults() {
        const results = this.textResults
        .getText()
        .trim()
        .replace("product(s) found", "");
        return parseInt(results);
    };

    searchTermUsed() { 
        return this.textSearchTerm
        .getText()
        .trim()
        .replace("Search on:", "");
    };
};

module.exports = new ListingPage();