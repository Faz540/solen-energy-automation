const waitUntil = require("../utils/waits");
const listingPage = require("./listing.spec");
const loginPage = require("./login.spec");
const customerServicePage = require("./customerService.spec");

class Menu {
    get imgLogo() { return $("[alt='Logo image']"); }
    get linkCustomerServices() { return $("[href='/service']"); }
    get linkLogin() { return $("=Login"); }
    get inputSearch() { return $("#searchbox"); }
    get buttonSearch() { return $(".btn-search"); }
    get linkBasket() { return $("[href='/shop/basket']"); }
    get linkHome() { return $("[href='/en-us/']"); }
    get linkSolar() { return $("[href='/solar']"); }
    get linkAccount() { return $("[href='https://www.solenenergy.com/profile/']"); }
    get linkDelivery() { return $("[href='/service/delivery']"); }
    get linkAboutUs() { return $("[href='/about-us']"); }
    get linkContactUs() { return $("[href='/contact-us']"); }
    get linkNews() { return $("[href='/news/']"); }

    searchFor(searchTerm) {
        this.inputSearch.setValue(searchTerm);
        this.buttonSearch.click();
        return waitUntil.elementIsVisible(listingPage.textHeader);
    };

    navigateToLoginPage() {
        this.linkLogin.click();
        return waitUntil.elementIsVisible(loginPage.textHeader);
    };

    navigateToCustomerServicesPage() {
        this.linkCustomerServices.click();
        return waitUntil.elementIsVisible(customerServicePage.divPageContent);
    };
};

module.exports = new Menu();