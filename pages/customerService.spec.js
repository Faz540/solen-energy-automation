const waitUntil = require("../utils/waits");

class CustomerService {
    get divPageContent() { return $(".fr-view"); }

    open() {
        browser.url("/service");
        return waitUntil.elementIsVisible(this.divPageContent);
    };
};

module.exports = new CustomerService();