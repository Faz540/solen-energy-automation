const waitUntil = require("../utils/waits");

class LoginPage {
    get textHeader() { return $(".h1-holder"); } 
    get inputEmail() { return $("#UserName"); }
    get inputPassword() { return $("#Password"); }
    get buttonLogin() { return $(".btn-login"); }

    open() {
        browser.url("/profile/login");
        return waitUntil.elementIsVisible(this.textHeader);
    };

    login(email = "training@solenenergy.com", password = "demosana") {
        this.inputEmail.setValue(email);
        this.inputPassword.setValue(password);
        const loginPageUrl = browser.getUrl();
        this.buttonLogin.click();
        return waitUntil.pageURLIsNoLonger(loginPageUrl);
    };
};

module.exports = new LoginPage();