const expect = require("chai").expect;

const actions = require("../utils/actions");

const homePage = require("../pages/home.spec");
const menu = require("../pages/menu.spec");
const listingPage = require("../pages/listing.spec");
const loginPage = require("../pages/login.spec");

describe("Smoke Test", function() {
    beforeEach(function() {
        homePage.open();
    });
    it("confirms Homepage tabs are present", function() {
        expect(menu.linkHome.isVisible()).to.be.true;
        expect(menu.linkSolar.isVisible()).to.be.true;
        expect(menu.linkAccount.isVisible()).to.be.true;
        expect(menu.linkDelivery.isVisible()).to.be.true;
        expect(menu.linkAboutUs.isVisible()).to.be.true;
        expect(menu.linkContactUs.isVisible()).to.be.true;
        expect(menu.linkNews.isVisible()).to.be.true;
    });
    it("confirms the 'Customer Service' link is present and functioning", function() {
        expect(menu.linkCustomerServices.isVisible()).to.be.true;
        menu.navigateToCustomerServicesPage();
        const currentUrl = actions.getPageUrl();
        expect(currentUrl).to.equal("https://www.solenenergy.com/service");
    });
    it("confirms the 'Login' link is present and functioning", function() {
        expect(menu.linkLogin.isVisible(), "Home Page: Login link is not present.").to.be.true;
        menu.navigateToLoginPage();
        const currentUrl = actions.getPageUrl();
        expect(currentUrl, "Login Page: User was not navigated to the correct page.").to.contain("https://www.solenenergy.com/profile/login");
    });
    it("confirms the user can successfully log in", function() {
        menu.navigateToLoginPage();
        loginPage.login();
        const cookies = actions.getCookies();
        expect(cookies, "Login: .ASPXAUTH_SS cookie does not exist.").to.contain(".ASPXAUTH_SS");
        expect(cookies, "Login: .ASPXAUTH_SS_s cookie does not exist.").to.contain(".ASPXAUTH_SS_s");
        actions.deleteCookies();
    });
    it("performs a search and expects results to be returned", function() {
        menu.searchFor('x1');
        expect(listingPage.textNoResults.isExisting(), "Listing Page: No results were returned.").to.be.false;
        const numberOfResults = listingPage.numberOfResults();
        const numberOfProductsPerPage = listingPage.numberOfProductsPerPage();
        expect(numberOfResults, "Listing Page: 0 Results").to.be.greaterThan(0);
        expect(numberOfProductsPerPage, "Listing Page: No products are visible on the page.").to.be.greaterThan(0);
    });
});